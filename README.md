﻿Bootstraped with Create React App

You can get the GIT repo of this code test at [https://gitlab.com/karlasbcn/basketfriends](https://gitlab.com/karlasbcn/basketfriends)

# How to run

Before start,  install the NodeJS dependencies with `yarn install`

`yarn start` 
Run the development web server (http://localhost:3000)

`yarn run storybook` 
Run the storybook to check UI for some components (http://localhost:9009)

`yarn test`
 Run unit tests

You can obviously replace `yarn` with `npm`

# Used libraries (beside React)


## Redux

Even there are not too much different models, there's one that is transversal to almost whole app: `user`, since its id and auth token are used frequently. Then it's good to have an state accessible from any part of the app, being connected to some component container, or being called directly via `store`. That´s why we are using it instead of letting React state handle them.
As support for async and composed actions, Redux-Thunk has been added.

## React Router

The usual library to handle routes. Also useful to track which type of action triggered a route change

## Axios
HTTP requests. We could rely on native `fetch` API,  but Axios offers a good experience when handling error requests

## Styled Components
Great style solution for having the styles definition along with the logic definition of a component. Also great for keep quite complex components clean from DOM and CSS stuff.
In addition, we can forget about setting up SASS, Stylus, etc...


# Code structure

All code has been distributed in different folders:

## Actions
Redux actions. Beside the redux obvious dispatches, actions are the only one modules who imports from the API module. That means API requests are only called from the actions

## API
In one hand, it contains the generic HTTP request module, and its different flavours for GET and POST. On the other, we are exporting the different app API calls by exporting the previous one. This way it is very easy to define and read the different endpoints used by the application

## Components
Here we have transversal components (like buttons or inputs), or more specific ones. Only place where styles (with Styled Components) and DOM are involved. 
Components defined here can be compositions from other components, or have very simple logic (like Form, where default onSumbit calls are prevented)

## Containers
Transversal components, always on the very top level of the app component hierarchy. They are two: 

`Router` , the top component, which defines the app routes, and handles the app auth (can't access on a private page if auth is not set). Note that the `'/'` route match two pages, selecting one depending on the auth state (logged or not). Router also redirects to this route in case path is not defined. In case some API error occurs, also renders the error message instead of the routes (see below)

All app route components are wrapped with the `Page` component, which wraps the navigation bar (which is different if user is logged or not), common for all app pages.

## Pages
The different pages of the app. They are connected to the redux state, so it could be named "containers" as well. Pages are responsable of the page content, not the navigation bar.

All pages when mount perform a fetch action to get the data to display, and invalidates it once it gets unmounted. Reason is to prevent fetch once and always rely on that data. Friends and conversations can be updated during the user session.

## Reducers
Redux reducers, one for each data model. Its index also exports the redux store.

## Stories
Different stories for storybook. A component has its own file.

## Tests
Unit tests. Every feature (component, redux action/reducer...) has its own file.

## Utils
A place to set any small module that don´t match on the previous list... Just contains the Styled Components theme, and generic validation module.


# Auth

When user logs in, token is stored in redux state, and in `localStorage`. When token is needed to perform an authenticated API request, it's always read from the store.

When app is restarted,  it checks if the token is present in the localStorage. In that case, it´s copied to the redux state, and user is fetched.

We assume the token never expires and is always valid, but we could check it if the user fetch fails because of a 403 error, cancelling the auth.

When user logs out, token is removed from the redux state and the localStorage.

# API error handling

We assume that an API request has failed because of some server error if it does not return a 200 status code. In that case, the API module catches the fail, and sets the error flag (in redux) to true. Router renders an error message in this case. You can test this by forcing to fetch a not existing resource (like http://localhost:3000/conversation/9999999)

There's one exception: the `/authenticate/` endpoint. In this case we catch the not valid credentials error, and render a feedback message on the login form.

# Page transitions

Instead of relying in some third party libraries like `react-transition-group`, since animation is simple, a CSS transition has been implemented on the `Content` components (which wraps the page contents, without the navigation bar). A prop is passed to the component telling if the route transition was a back, or not.

# Models

Since entities are so basic, no special classes or models have been set. Friends, conversations and messages are arrays of plain objects stored on the redux state.

# Testing

There are two different tests: Unit testing and Storybook

Unit testing does not need explanation... It has been done with Jest (which is included in `create-react-app` and Enzyme to test React components.

Storybook is useful to develop and test from a UI perspective the React components in an isolated way.

As you can see, very few tests have been implemented... Sorry, it was a matter of time... But included them just to show how is it implemented.

# Isomorphic rendering

Not done. Once again, matter of time...
If done, I would wrap the code (except the router) in a Next.js application.









