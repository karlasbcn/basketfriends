import * as api from '../api';

export const SET_USER    = 'SET_USER'
export const UNSET_USER  = 'UNSET_USER'
export const SET_TOKEN   = 'SET_TOKEN'

const setUser   = user => ({ type: SET_USER, user })
const setToken  = token => ({ type: SET_TOKEN, token })
const unsetUser = () => ({ type: UNSET_USER })

export const login = (email, password) => {
  return function(dispatch){
    return new Promise(function(resolve){
      api.authenticate(email, password).then(function(token){
        if (token){
          dispatch(setToken(token))
          localStorage.setItem('TOKEN', token)
          dispatch(setSession(token))
        }
        resolve(!!token)
      });
    })
  }
}

export const logout = () => {
  return function(dispatch){
    localStorage.removeItem('TOKEN')
    dispatch(unsetUser())
  }
}

export const checkSession = () => {
  return function(dispatch){
    const token = localStorage.getItem('TOKEN')
    !!token && dispatch(setSession(token))
  }
}

export const setSession = token => {
  return function(dispatch){
    dispatch(setToken(token))
    api.getUser().then(user => dispatch(setUser(user)))
  }
}

export const signUp = (name, email, password) => {
  return function(dispatch){
    api.signUp(name, email, password).then(() => dispatch(login(email, password)))
  }
}