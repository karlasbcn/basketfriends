import { getConversations } from '../api'

export const SET_CONVERSATIONS        = 'SET_CONVERSATIONS'
export const INVALIDATE_CONVERSATIONS = 'INVALIDATE_CONVERSATIONS'

const setConversations = conversations => ({ type: SET_CONVERSATIONS, conversations })
export const invalidateConversations = () => ({ type: INVALIDATE_CONVERSATIONS })

export const fetchConversations = () => {
  return function(dispatch){
    getConversations().then(conversations => dispatch(setConversations(conversations)))
  }
}