import { getFriends } from '../api'

export const SET_FRIENDS        = 'SET_FRIENDS'
export const INVALIDATE_FRIENDS = 'INVALIDATE_FRIENDS'

const setFriends = friends => ({ type: SET_FRIENDS, friends })
export const invalidateFriends = () => ({ type: INVALIDATE_FRIENDS })

export const fetchFriends = () => {
  return function(dispatch){
    getFriends().then(friends => dispatch(setFriends(friends)))
  }
}