import * as api from '../api'

export const SET_CHAT        = 'SET_CHAT'
export const INVALIDATE_CHAT = 'INVALIDATE_CHAT'
export const ADD_MESSAGE     = 'ADD_MESSAGE'

const setChat = chat => ({ type: SET_CHAT, chat })
export const addMessage = message => ({ type: ADD_MESSAGE, message })
export const invalidateChat = () => ({ type: INVALIDATE_CHAT })

export const addChat = friendId => {
  return function(dispatch){
    api.getConversations().then(conversations => {
      const prevConversation = conversations.find(conversation => conversation.participants.some(p => p.id === friendId))
      const fetchConversation = () => {
        return !!prevConversation ? api.getSingleConversation(prevConversation.id) : api.addConversation(friendId)
      }
      fetchConversation().then(({ id, messages, participants }) => {
        const friend = participants.find(participant => participant.id === friendId).name
        api.setConversationAsRead(id).then(() => dispatch(setChat({ id, messages, friend })))
      })
    })
  }
}

export const sendMessage = text => {
  return function(dispatch, getState){
    api.sendMessage(getState().chat.id, text).then(message => dispatch(addMessage(message)))
  }
}