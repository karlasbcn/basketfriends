import React        from 'react';
import ReactDOM     from 'react-dom';
import { Provider } from 'react-redux';

import Router       from './containers/Router'
import store        from './reducers';

ReactDOM.render(
  <Provider store={ store }>
    <Router />
  </Provider>, 
  document.getElementById('root')
)
