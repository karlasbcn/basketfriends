const theme = {
  orange : '#f4801a',
  green  : '#40b27b',
  grey   : '#dedede',
  conversation : {
    self   : '#e8eaed',
    friend : '#d1e2fc'
  }
}

export default theme;