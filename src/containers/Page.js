import React, { Fragment }    from 'react';
import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect }           from 'react-router-dom';

import { logout } from '../actions/user'
import Body       from '../components/Body'
import NavBar     from '../components/NavBar'
import NavLink    from '../components/NavLink';
import Content    from '../components/Content';
import Error      from '../components/Error';

const navLinks = {
  auth : {
    "Messages" : '',
    "Friends"  : 'friends',
    "signout"  : true
  },
  notAuth : {
    "Login"   : '',
    "Sign Up" : 'signup',
  }
}

class Page extends React.Component{
  renderLinks(){
    const { logout, isLogged, route } = this.props
    const auth  = navLinks[isLogged ? 'auth' : 'notAuth']
    const links = Object.keys(auth).map((text, i) => {
      if (text === 'signout'){
        return <NavLink key={ i } isLogged signout={ logout } />
      }
      const path = '/' + auth[text]
      return <NavLink key={ i } isLogged={ isLogged } current={ route === path } to={ path }>{ text }</NavLink>
    })
    return <Fragment>{ links }</Fragment>
  }
  render(){
    const { isLogged, children, authRequired, route, apiError, routerAction } = this.props;
    if (apiError){
      return (
        <Body>
          <Error />
        </Body>
      )
    }
    if ( route !== '/' && authRequired !== isLogged ){
      return <Redirect to="/" />
    }
    return (
      <Body>
        <NavBar isLogged={ isLogged } >
          { this.renderLinks() }
        </NavBar>
        <Content action={ routerAction }>
          { children }
        </Content>
      </Body>
    )
  }
}

const mapStateToProps = state => ({
  isLogged : !!state.user.token,
  apiError : state.error
})
const mapDispatchToProps = dispatch => bindActionCreators({ logout }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Page);
