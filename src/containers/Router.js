import React                     from 'react'
import { BrowserRouter, Switch, 
         Route, Redirect }       from 'react-router-dom'
import { connect }               from 'react-redux';
import { bindActionCreators }    from 'redux';

import Page             from './Page'
import Login            from '../pages/Login'
import SignUp           from '../pages/SignUp'
import Messages         from '../pages/Messages'
import Friends          from '../pages/Friends'
import Conversation     from '../pages/Conversation'
import { checkSession } from '../actions/user';

const page = (component, authRequired = true) => props => (
  <Page routerAction={ props.history.action } route={ props.match.path } authRequired={ authRequired }>
    { React.createElement(component, props) }
  </Page>
)

class Router extends React.Component{
  componentWillMount(){
    this.props.checkSession()
  }
  render(){
    const { isLogged } = this.props
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/conversation/:friendId" component={ page(Conversation) } />
          <Route exact path="/friends" component={ page(Friends) } />
          <Route exact path="/signup" component={ page(SignUp, false) } />
          <Route exact path="/" component={ page(isLogged ? Messages : Login, isLogged) } />
          <Route component={ () => <Redirect to="/" /> } />
        </Switch>
      </BrowserRouter>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ checkSession }, dispatch)
export default connect(state => ({ isLogged : !!state.user.token }), mapDispatchToProps)(Router);