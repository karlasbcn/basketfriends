export const conversation = {
  "id":202,
  "messages":[
    {"id":52,"sender":{"id":6,"name":"luis"},"text":"Hey Carles!"},
    {"id":53,"sender":{"id":12,"name":"Carles Pagès Rozas"},"text":"Hey Luis!"},
    {"id":56,"sender":{"id":12,"name":"Carles Pagès Rozas"},"text":"Hi?"}
  ],
  "friend":"luis"
}

export const userId = 6

export const chatPageActions = {
  addChat : () => true, 
  invalidateChat : () => true, 
  sendMessage : () => true
}

export const chatPageMatch = { params : { friendId : 12 } }