import { addMessage } from '../actions/chat'
import reducer from '../reducers/chat'
import { conversation } from './mocks'

describe('Chat redux', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(null)
  })
  it('Should increase by one chat messages number after adding a message', () => {
    const newState = reducer(conversation, addMessage('Hello'))
    expect(newState.messages.length).toEqual(conversation.messages.length + 1)
  })
})