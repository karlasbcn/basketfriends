import React from 'react';
import { Chat } from '../pages/Conversation';
import Body from '../components/Body'
import { ChatMessage } from '../components/Chat'
import { conversation, chatPageActions, chatPageMatch, userId } from './mocks'

const component = mount(
  <Body>
    <Chat userId={ userId } { ...chatPageActions } chat={ conversation } match={ chatPageMatch } />
  </Body>
)

describe('Conversation Page', function(){

  it('Should render as message boxes as messages on conversation', function(){
    expect(component.find(ChatMessage)).toHaveLength(conversation.messages.length);
  })
});