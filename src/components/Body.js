import React    from 'react'
import styled, { ThemeProvider } from 'styled-components'
import theme from '../utils/theme'

const Styled = styled.div`
  @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700');
  font-family: 'Source Sans Pro';
`
const Body = ({ children }) => (
  <ThemeProvider theme={ theme }>
    <Styled>
      { children }
    </Styled>
  </ThemeProvider>
)

export default Body;
