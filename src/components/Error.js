import React    from 'react'
import styled   from 'styled-components'

import Button from './Button'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: red;
`

const Text = styled.div`
  color: white;
  text-align: center;
  font-size: 36px;
`

const Error = () => (
  <Container>
    <Text>Ooops! That was an API error... Check console</Text>
    <Text>Perhaps you want to reload this window?</Text>
    <Button green onClick={ () => window.location.reload() } >Reload</Button>
  </Container>
)

export default Error
