import React    from 'react'
import styled   from 'styled-components'
import { Link } from 'react-router-dom'

import Button   from './Button'

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  border: 2px solid ${ ({ theme }) => theme.grey };
  align-items: center;
  background-color: ${ ({ theme }) => theme.grey };
  border-top: none;
  width: 100%;
  position: relative;
  margin-top: 3em;
  & a {
    width: 100%;
    display: flex;
    justify-content: center;
    &:visited, &:link, &:hover, &:active{
      text-decoration: none;
    }
  }
  & button {
    max-width: 200px;
    width: 100%;
  }
`

const Name = styled.div`
  height: 3em;
  text-align: center;
  line-height: 3em;
  border: 2px solid ${ ({ theme }) => theme.grey };
  background-color: white;
  width: 100%;
`

const Text = styled.div`
  width: 70%;
  height: auto;
  text-align: center;
  padding: 1em;
  font-weight: ${ ({ isUnread }) => isUnread ? 'bold' : 'auto' };
  min-height: 3em;
`

const Unread = styled.div`
  position: absolute;
  background-color: ${ ({ theme }) => theme.orange };
  height: 2em;
  width: 2em;
  border-radius: 2em;
  color: white;
  left: -1.5em;
  top: -1.5em;
  font-size: 10px;
  padding: 1em;
  line-height: 2em;
`

const Conversation = ({ is_unread, last_message, friend }) => (
  <Wrap>
    { is_unread && <Unread>New</Unread> }
    <Name>{ friend.name }</Name>
    <Text isUnread={ is_unread } >{ last_message }</Text>
    <Link to={ '/conversation/' + friend.id } >
      <Button small>Reply</Button>
    </Link>
  </Wrap>
)

export default Conversation;