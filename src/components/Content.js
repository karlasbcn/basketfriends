import React from 'react'
import styled from 'styled-components'

const transitionTime = 200

const Styled = styled.div`
  width: 75%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  max-width: 800px;
  transform: translateX(${ ({ out, fromLeft }) => out ? (5000 * (fromLeft ? -1 : 1)) : 0 }px);
  transition: transform ${ transitionTime }ms ease-out;
`
class Content extends React.Component{
  state={ out : true }
  componentDidMount(){
    setTimeout(() => !this.unmount && this.setState({ out : false }), transitionTime)
  }
  componentWillUnmount(){
    this.unmount = true
  }
  render(){
    const { action, children } = this.props
    return (
      <Styled out={ this.state.out } fromLeft={ action === 'PUSH' }>
        { children }
      </Styled>
    )
  }
}

export default Content