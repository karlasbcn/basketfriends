import styled from 'styled-components'

const Input = styled.input`
  padding: 1em;
  margin : 1em;
  outline: none;
  font-family: 'Source Sans Pro';
  font-size: 18px;
`

export default Input