import React  from 'react';
import styled from 'styled-components'

const Styled = styled.button`
  padding: ${ ({ small }) => small ? 0.5 : 1 }em;
  margin: 1em;
  outline: none;
  font-family: 'Source Sans Pro';
  font-size: ${ ({ small }) => small ? 14 : 24 }px;
  background-color : ${ ({ theme, green }) => theme[green ? 'green' : 'orange'] };
  opacity : ${ ({ disabled }) => disabled ? 0.4 : 1 };
  color: white;
  border-radius: 6px;
  cursor: ${ ({ disabled }) => disabled ? 'default' : 'pointer' };
`

const Button = ({ onClick, disabled, children, green, small }) => (
  <Styled small={ small } green={ green } disabled={ disabled } onClick={ disabled ? (() => {}) : onClick }>
    { children }
  </Styled>
)

export default Button