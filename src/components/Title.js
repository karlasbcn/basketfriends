import styled from 'styled-components'

const Title = styled.h1`
  font-size: ${ ({ small }) => small ? 24 : 36 }px;
  font-weight: normal;
  text-align: center;
  margin: 0.25em;
  ${ ({ small }) => small ? '' : 'margin-top: 1em;' };
  color : ${ ({ theme, green, orange }) => green ? theme.green : (orange ? theme.orange : 'black') };
`

export default Title