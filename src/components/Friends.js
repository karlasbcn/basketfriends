import React    from 'react'
import styled   from 'styled-components'
import { Link } from 'react-router-dom'

import Button   from './Button'

export const FriendContainer = styled.div`
  display: flex;
  flex-direction: column;
  border: 2px solid ${ ({ theme }) => theme.grey };
  margin-top: 3em;
  width: 100%;
`

const Wrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom: 2px solid ${ ({ theme }) => theme.grey };
  &:last-child{
    border-bottom: none;
  }
`

const Name = styled.span`
  margin-left: 1em
`

export const Friend = ({ name, id }) => (
  <Wrap>
    <Name>{ name }</Name>
    <Link to={'/conversation/' + id }>
      <Button small>Send message</Button>
    </Link>
  </Wrap>
)