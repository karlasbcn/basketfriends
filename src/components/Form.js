import React  from 'react';
import styled from 'styled-components'

const Styled = styled.form`
  display: flex;
  flex-direction: column;
  padding: 12px;
  width: 100%;
  max-width: 500px;
  align-self: center;
`

class Form extends React.Component{
  submit(e){
    e.preventDefault()
    this.props.onSubmit()
  }
  render(){
    return (
      <Styled onSubmit={ e => this.submit(e) } >
        { this.props.children }
      </Styled>
    )
  }
}

export default Form;