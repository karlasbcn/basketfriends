import styled from 'styled-components'

const NavBar = styled.div`
  display: flex;
  justify-content: flex-${ ({ isLogged }) => isLogged ? 'start' : 'end' };
  width: 100%;
  background-color: ${ ({ isLogged, theme }) => isLogged ? theme.grey : 'white' }
`

export default NavBar