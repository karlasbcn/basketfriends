import styled from 'styled-components'

const BadCredentials = styled.div`
  opacity: ${ ({ show }) => show ? 1 : 0 };
  transition: opacity 0.2s linear;
  color: red;
  font-size: 14px;
  text-align: center;
`

export default BadCredentials
