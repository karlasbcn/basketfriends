import React    from 'react';
import styled   from 'styled-components'
import { Link } from 'react-router-dom';

const Container = styled.div`
  font-size: 20px;
  margin: 1em;
  ${ ({ signout }) => signout ? `color:black; margin-left: auto; cursor:pointer` : '' }
  & a{
    &:visited, &:link, &:hover, &:active{
      text-decoration: none;
      color : ${ ({ theme, isLogged, current }) => current ? (isLogged ? theme.green : theme.orange) : 'black' };
    }
  }
  &:last-child{
    margin-right: 2em;
  }
`

const SignoutLink = ({ onClick }) => <span onClick={ onClick }>Logout</span>

const RegularLink = ({ to, text }) => (
  <Link to={ to }>
    { text }
  </Link>  
)

const NavLink = ({ to, signout, current, isLogged, children }) => (
  <Container isLogged={ isLogged } current={ current } signout={ !!signout }>
    { signout ? <SignoutLink onClick={ signout } /> : <RegularLink text={ children } to={ to } /> }
  </Container>
)

export default NavLink;