import React    from 'react'
import styled   from 'styled-components'

import Button from './Button'

export const MessagesContainer = styled.div`
  margin: 2.5em 0;
`

const ChatMessageWrap = styled.div`
  display: flex;
  flex-direction: row${ ({ isFromUser }) => isFromUser ? '-reverse' : '' };
  margin: 0.5em 0;
`

const Name = styled.div`
  height: 3em;
  text-align: center;
  background-color: white;
  width: 20%;
  height: 100%;
  ${ ({ isFromUser }) => isFromUser ? 'margin-left' : 'margin-right' }: 2em;
  margin-top: 0.7em;
`

const Text = styled.div`
  background-color: ${ ({ theme, isFromUser }) => theme.conversation[ isFromUser ? 'self' : 'friend' ] };
  width: 65%;
  padding: 1em;
  min-height: 2em;
  position: relative;
  font-weight: ${ ({ isFromUser }) => isFromUser ? 'normal' : 'bold' };
  &:after, &:before {
    top: 1em;
    ${({ arrowDirection }) => arrowDirection }: 100%;
    border: solid transparent;
    content: " ";
    position: absolute;
    border-${({ arrowDirection }) => arrowDirection }-color: ${ ({ theme, color }) => theme.conversation[color] };
    border-width: 0.5em;
  }
`

export const ChatMessage = ({ name, text, isFromUser }) => (
  <ChatMessageWrap isFromUser={ isFromUser } >
    <Name isFromUser={ isFromUser }>{ name }</Name>
    <Text isFromUser={ isFromUser } arrowDirection={ isFromUser ? 'left' : 'right' }>{ text }</Text>
  </ChatMessageWrap>
)

const TextArea = styled.textarea`
  border: 1px solid ${ ({ theme }) => theme.grey };
  height: 8em;
  resize: none;
  font-size: 16px;
  font-family: 'Source Sans Pro';
  outline: none;
  padding: 1em;
  margin-top: 1em;
`
const SendBoxWrap = styled.div`
  display: flex;
  flex-direction: column;
  & button{
    align-self: flex-end;
    margin-right: 0;
    font-size: 20px;
  }
`
export const SendBox = ({ message, onChange, onSend }) => (
  <SendBoxWrap>
    <TextArea onChange={ e => onChange(e.target.value) } value={ message } placeholder="Write your message here..." />
    <Button small disabled={ !message } onClick={ message ? onSend : () => {} }>Send Message</Button>
  </SendBoxWrap>
)

const BackContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  color: ${ ({ theme }) => theme.orange }; 
  font-size: 22px;
  position: absolute;
  margin-left: -12%;
  margin-top: 1em;
  cursor: pointer;
  @media only screen and (min-width: 1100px) {
    margin-left: -14%;
  }
`

const Arrow = styled.span`
  margin-right: 0.5em;
`

const BackText = styled.span`
  @media only screen and (max-width: 500px) {
    display: none;
  }
`

export const Back = ({ onClick }) => (
  <BackContainer onClick={ onClick }>
    <Arrow>{ '<<' }</Arrow>
    <BackText>Back</BackText>
  </BackContainer>
)
