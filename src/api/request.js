import axios from 'axios'
import store from '../reducers';
import { showApiError } from '../actions/error'

const getToken = () => store.getState().user.token

const request = (path, method, data, auth, catchError) => new Promise(function(resolve, reject){
  let headers = { 'Content-Type': 'application/json' }
  const url   = '/api/rest' + path
  if (auth){
    headers['Authorization'] = 'Token ' + getToken() 
  }
  axios.request({ url, data, method, headers })
    .then(response => resolve(response.data))
    .catch(error => {
      if (catchError){
        reject(error)
      }
      else{
        console.error(error)
        store.dispatch(showApiError())
      }
    })
})

export const post = (path, data = {}, auth = true, catchError = false) => request(path, 'post', data, auth, catchError);
export const get  = path => request(path, 'get', {}, true, false);

export const apiError = error => {
  console.error(error)
  store.dispatch(showApiError())
}
