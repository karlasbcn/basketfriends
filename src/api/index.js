
import { post, get, apiError } from './request';

export const authenticate = (username, password) => new Promise(function(resolve){
  post('/authenticate/', { username, password }, false, true)
    .then(response => resolve(response.token))
    .catch(error => !!error.response.data.non_field_errors ? resolve(false) : apiError())
});

export const signUp = (name, email, password) => post('/signup/', { name, email, password }, false)
export const getUser = () => get('/user/')
export const getFriends = () => get('/friends/')
export const getConversations = () => get('/conversations/')
export const getSingleConversation = id => get(`/conversations/${ id }/`)
export const addConversation = friend_id => post('/conversations/', { friend_id })
export const setConversationAsRead = id => post(`/conversations/${ id }/read/`)
export const sendMessage = (id, text) => post(`/conversations/${ id }/`, { text })
