import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions';

import Button from '../components/Button'
import Body from '../components/Body'

const fakeClick = action('Clicked!')

storiesOf('Button', module)
  .addDecorator(story => (
      <Body>
        <div style={{ width: 200 }} >
          { story() }
        </div>
      </Body>
  ))
  .add('Default button', () => <Button onClick={ fakeClick }>Hello</Button>)
  .add('Small button', () => <Button onClick={ fakeClick } small>Hi</Button>)
  .add('Disabled', () => <Button onClick={ fakeClick } disabled>I'm disabled</Button>)


