import React from 'react'
import { BrowserRouter } from 'react-router-dom'

import { storiesOf } from '@storybook/react'

import Conversation from '../components/Conversation'
import Body from '../components/Body'
import Content from '../components/Content'

const fakeLongMessage  = 'Elige el destino y la fecha de tu próximo vuelo. Puedes añadir requisitos adicionales para tu búsqueda como: clase, número de viajeros adultos, niños, etc.'
const fakeShortMessage = 'Scotty, beam me up!'
const fakeFriend       = { name : 'James T. Kirk' }

storiesOf('Conversation', module)
  .addDecorator(story => (
    <BrowserRouter>
      <Body>
        <Content>
          { story() }
        </Content>
      </Body>
    </BrowserRouter>
  ))
  .add('With long message', () => <Conversation last_message={ fakeLongMessage } friend={ fakeFriend } />)
  .add('With short message', () => <Conversation last_message={ fakeShortMessage } friend={ fakeFriend } />)
  .add('Unread', () => <Conversation is_unread last_message={ fakeShortMessage } friend={ fakeFriend } />)


