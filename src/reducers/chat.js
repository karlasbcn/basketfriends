import { SET_CHAT, INVALIDATE_CHAT, ADD_MESSAGE } from '../actions/chat';
 
export default function (state = null, action) {
  switch (action.type) {
    case SET_CHAT:
      return action.chat
    case ADD_MESSAGE:
      return { ...state, messages : [ ...state.messages, action.message ] }
    case INVALIDATE_CHAT:
      return null
    default:
      return state
  }
}