import { SET_FRIENDS, INVALIDATE_FRIENDS } from '../actions/friends';
 
export default function (state = null, action) {
  switch (action.type) {
    case SET_FRIENDS:
      return action.friends.sort((a, b) => a.name > b.name)
    case INVALIDATE_FRIENDS:
      return null
    default:
      return state
  }
}