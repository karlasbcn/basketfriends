import { SET_USER, UNSET_USER, SET_TOKEN } from '../actions/user';

const initialState = { token : null }
 
export default function (state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      return { ...state, ...action.user }
    case SET_TOKEN:
      return { ...state, token : action.token }
    case UNSET_USER:
      return initialState
    default:
      return state
  }
}