import { SET_CONVERSATIONS, INVALIDATE_CONVERSATIONS } from '../actions/conversations';
 
export default function (state = null, action) {
  switch (action.type) {
    case SET_CONVERSATIONS:
      return action.conversations
        .filter(conversation => conversation.last_message !== null)
        .sort((a, b) => (a.is_unread === b.is_unread) ? 0 : (a.is_unread ? -1 : 1))
    case INVALIDATE_CONVERSATIONS:
      return null
    default:
      return state
  }
}