import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunkMiddleware                                   from 'redux-thunk'

import user from './user';
import friends from './friends';
import conversations from './conversations';
import chat from './chat';
import error from './error';

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore)

const reducers = combineReducers({ user, friends, conversations, chat, error })
const store    = createStoreWithMiddleware(reducers)

window.devRedux = () => store.getState();

export default store;