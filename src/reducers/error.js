import { SHOW_API_ERROR } from '../actions/error';
 
export default function (state = false, action) {
  switch (action.type) {
    case SHOW_API_ERROR:
      return true
    default:
      return state
  }
}