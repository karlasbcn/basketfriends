import React, { Fragment }    from 'react';
import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';

import { isEmail } from '../utils/validation'
import { login }   from '../actions/user'
import Button      from '../components/Button'
import Input       from '../components/Input'
import Form        from '../components/Form'
import Title       from '../components/Title'
import BadCredentials from '../components/BadCredentials'

class Login extends React.Component{
  state = {
    email : '',
    password : '',
    showError : false
  }
  isInvalid(){
    const { email, password } = this.state
    return !(isEmail(email) && password.length)
  }
  updateField(field, event){
    this.setState({ [field] : event.target.value })
  }
  submit(){
    const { email, password } = this.state
    if (this.isInvalid()){
      return
    }
    this.props.login(email, password).then(ok => {
      if (!ok){
        this.setState({ showError : true })
        setTimeout(() => this.setState({ showError : false }), 3000);
      } 
    });
  }
  render(){
    const { email, password, showError } = this.state
    return(
      <Fragment>
        <Title orange>Welcome back</Title>
        <Title small>Please login</Title>
        <Form onSubmit={ this.submit.bind(this) } >
          <Input type="text" onChange={ e => this.updateField('email', e) } value={ email } placeholder="Enter email address" />
          <Input type="password" onChange={ e => this.updateField('password', e) } value={ password } placeholder="Enter password" />
          <Button type="submit" disabled={ this.isInvalid() }>Login</Button>
          <BadCredentials show={ showError }>Unable to log in with provided credentials</BadCredentials>
        </Form>
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch)
export default connect(() => ({}), mapDispatchToProps)(Login)
