import React, { Fragment }    from 'react'
import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchConversations, invalidateConversations } from '../actions/conversations'
import Title from '../components/Title'
import Conversation from '../components/Conversation'

class Messages extends React.Component{
  componentDidMount(){
    this.props.fetchConversations()
  }
  componentWillUnmount(){
    this.props.invalidateConversations()
  }
  render(){
    const { conversations, userId } = this.props
    if (conversations === null){
      return null
    }
    if (!conversations.length){
      return <Title>You have not any conversation! Start a new one by pressing on "Friends"!</Title>
    }
    const conversationBoxes = conversations.map(({ id, participants, is_unread, last_message }) => {
      const friend = participants.find(participant => participant.id !== userId)
      return <Conversation key={ id } friend={ friend } is_unread={ is_unread } last_message={ last_message.text } />
    })
    return (
      <Fragment>
        <Title>Messages</Title>
        { conversationBoxes }
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  conversations : state.conversations,
  userId        : state.user.id
})

const mapDispatchToProps = dispatch => bindActionCreators({ fetchConversations, invalidateConversations }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Messages)