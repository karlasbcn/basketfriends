import React, { Fragment }    from 'react'
import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';

import { addChat, invalidateChat, sendMessage } from '../actions/chat'
import Title from '../components/Title'
import { MessagesContainer, ChatMessage, SendBox, Back } from '../components/Chat'

export class Chat extends React.Component{
  state = { newMessage : '' }
  getFriendId(){
    return parseInt(this.props.match.params.friendId, 10)
  }
  componentDidMount(){
    this.props.addChat(this.getFriendId())
  }
  componentWillUnmount(){
    this.props.invalidateChat()
  }
  sendMessage(){
    const { newMessage } = this.state
    if (newMessage){
      this.props.sendMessage(newMessage)
      this.setState({ newMessage : '' })
    }
  }
  render(){
    const { chat, userId, history } = this.props
    if (chat === null){
      return null
    }
    const { messages, friend } = chat
    const chatMessages = messages.map( ({ id, sender, text }) => (
      <ChatMessage key={ id } isFromUser={ userId === sender.id } text={ text } name={ sender.name } />
    ))
    const sendBoxProps = {
      message  : this.state.newMessage,
      onChange : newMessage => this.setState({ newMessage }),
      onSend   : this.sendMessage.bind(this)
    }
    return (
      <Fragment>
        <Back onClick={ () => history.goBack() } />
        <Title>Chat with { friend }</Title>
        <MessagesContainer>
          { chatMessages }
        </MessagesContainer>
        <SendBox { ...sendBoxProps } />
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  chat   : state.chat,
  userId : state.user.id
})

const mapDispatchToProps = dispatch => bindActionCreators({ addChat, invalidateChat, sendMessage }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Chat)