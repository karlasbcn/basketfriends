import React, { Fragment } from 'react';
import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';

import { isEmail } from '../utils/validation'
import { signUp }   from '../actions/user'
import Button from '../components/Button'
import Input  from '../components/Input'
import Form   from '../components/Form'
import Title  from '../components/Title'

class SignUp extends React.Component{
  state = {
    name : '',
    email : '',
    password : '' 
  }
  isInvalid(){
    const { name, email, password } = this.state
    return !(isEmail(email) && name.length && password.length)
  }
  updateField(field, event){
    this.setState({ [field] : event.target.value })
  }
  submit(){
    if (this.isInvalid()){
      return;
    }
    const { name, email, password } = this.state
    this.props.signUp(name, email, password);
  }
  render(){
    const { name, email, password } = this.state
    return(
      <Fragment>
        <Title green>Sign up for BasketFriends!</Title>
        <Title small>Please enter your details</Title>
        <Form onSubmit={ this.submit.bind(this) } >
          <Input type="text" onChange={ e => this.updateField('name', e) } value={ name } placeholder="*Your name" />
          <Input type="text" onChange={ e => this.updateField('email', e) } value={ email } placeholder="*Your email address" />
          <Input type="password" onChange={ e => this.updateField('password', e) } value={ password } placeholder="*Choose a password" />
          <Button type="submit" green disabled={ this.isInvalid() }>Sign up</Button>
        </Form>
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ signUp }, dispatch)
export default connect(() => ({}), mapDispatchToProps)(SignUp)