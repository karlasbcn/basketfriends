import React, { Fragment }    from 'react'
import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchFriends, invalidateFriends } from '../actions/friends'
import Title from '../components/Title'
import { FriendContainer, Friend } from '../components/Friends'

class Friends extends React.Component{
  componentDidMount(){
    this.props.fetchFriends()
  }
  componentWillUnmount(){
    this.props.invalidateFriends()
  }
  render(){
    const { friends } = this.props
    if (friends === null){
      return null
    }
    if (!friends.length){
      return <Title>Unfortunately, you have no friends... (in this app)</Title>
    }
    return (
      <Fragment>
        <Title>Friends</Title>
        <FriendContainer>
          { friends.map(friend => <Friend key={ friend.id } { ...friend } />) }
        </FriendContainer>
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ fetchFriends, invalidateFriends }, dispatch)
export default connect(state => ({ friends :  state.friends }), mapDispatchToProps)(Friends)